# saaagagamez.dev

[Zur Webseite](https://de.saaagagamez.dev) <br>
[To the website](https://en.saaagagamez.dev)

## ToDo

- [ ] Make a ToDo List

## Changelog

### 2020-05-25 - Inital Commit
    - Initial Commit
    - Add/Update README.md
    - Add/Update Licence
    - Kontakt.html changed

### 2020-06-29
    - Updated Minecraft Plugins
    - Added Language Selection

### 2020-09-28
    - Add Gitlab-CI (Update Website automatically on push)
    - Add Dockerfile (Update Website automatically on push)

### 2020-09-29
    - Remove Minecraft Server
    - Move images to cdn
    - Move js to cdn
    - Move css to cdn
    - Fix invalid linkings
    - Add language icon to dropdown
    - Update README.md